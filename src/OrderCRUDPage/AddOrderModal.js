import { Alert, Box, Button, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";
function AddOrderModal({ openModalAdd, handleCloseAddModal, style, getData, refeshPage, setValueFilter, setValueFilterToEmpty }) {
    // khai báo các state
    const [drinksList, setDrinksList] = useState([])
    const [drinkValue, setDrinkValue] = useState("")

    const [size, setSize] = useState("S")
    const [duongKinh, setDuongKinh] = useState("20cm")
    const [suonNuong, setSuonNuong] = useState(2)
    const [salad, setSalad] = useState("200g")
    const [soLuongNuoc, setSoLuongNuoc] = useState(2)
    const [donGia, setDonGia] = useState(150000)
    const [loaiPizza, setLoaiPizza] = useState("Seafood")
    const [maVoucher, setMaVoucher] = useState(0)
    const [loaiNuocUong, setLoaiNuocUong] = useState("TRATAC")
    const [hoVaTen, setHoVaTen] = useState("")
    const [email, setEmail] = useState("")
    const [soDienThoai, setSoDienThoai] = useState(0)
    const [diaChi, setDiaChi] = useState("")
    const [loiNhan, setLoiNhan] = useState("")
    const [statusOrder, setStatusOrder] = useState("open")

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseAddModal()
    }
    // set lại các giá trị cần thiết mỗi khi size thay đổi
    const onSelectSizeChange = (event) => {
        if (event.target.value === "S") {
            setSize("S")
            setDuongKinh("20cm")
            setSuonNuong(2)
            setSalad("200g")
            setSoLuongNuoc(2)
            setDonGia(150000)
        }
        if (event.target.value === "M") {
            setSize("M")
            setDuongKinh("25cm")
            setSuonNuong(4)
            setSalad("300g")
            setSoLuongNuoc(3)
            setDonGia(200000)
        }
        if (event.target.value === "L") {
            setSize("L")
            setDuongKinh("30cm")
            setSuonNuong(8)
            setSalad("500g")
            setSoLuongNuoc(4)
            setDonGia(250000)
        }
    }

    // các hàm onchange bên dưới set lại state mỗi khi giá trị input thay đổi

    const onTypePizzaChange = (event) => {
        setLoaiPizza(event.target.value)
    }
    const onMaVoucherChange = (event) => {
        setMaVoucher(event.target.value)
    }
    const onDrinkTypeChange = (event) => {
        setLoaiNuocUong(event.target.value)
    }
    const onHoVaTenChange = (event) => {
        setHoVaTen(event.target.value)
    }
    const onEmailChange = (event) => {
        setEmail(event.target.value)
    }
    const onSoDienThoaiChange = (event) => {
        setSoDienThoai(event.target.value)
    }
    const onDiaChiChange = (event) => {
        setDiaChi(event.target.value)
    }
    const onLoiNhanChange = (event) => {
        setLoiNhan(event.target.value)
    }

    const onCreateOrderClick = () => {
        // thu thập dữ liệu
        var getDataFrom = {
            kichCo: size,
            duongKinh: duongKinh,
            suon: suonNuong,
            salad: salad,
            soLuongNuoc: soLuongNuoc,
            thanhTien: donGia,
            loaiPizza: loaiPizza,
            idVourcher: maVoucher,
            idLoaiNuocUong: loaiNuocUong,
            hoTen: hoVaTen,
            email: email,
            soDienThoai: soDienThoai,
            diaChi: diaChi,
            loiNhan: loiNhan,
        }
        // valid data
        var vCheckDataForm = validDataForm(getDataFrom)
        if (vCheckDataForm) {
            // gọi api tạo đơn hàng
            const body = {
                method: 'POST',
                body: JSON.stringify(getDataFrom),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
                .then((data) => {
                    handleCloseAddModal()
                    setStatusModal("success");
                    setNoidungAlertValid("Tạo đơn hàng thành công!");
                    setOpenAlert(true);
                    setValueFilter("")
                    setValueFilterToEmpty("")
                    refeshPage()
                })
                .catch((error) => {
                    setStatusModal("error");
                    setNoidungAlertValid("Tạo đơn hàng thất bại!")
                    setOpenAlert(true)
                })
        }
    }
    // hàm kiểm tra dữ liệu
    const validDataForm = (paramDataForm) => {
        const filterEmail = /^([a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?)$/;
        const filterPhone = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
        
        const filterPhone2 = /(([02]))+([0-9]{9})\b/
        if (paramDataForm.hoTen === "") {
            setStatusModal("error");
            setNoidungAlertValid("Họ tên cần được nhập vào!")
            setOpenAlert(true)
            return false
        }
        if ((filterEmail.test(paramDataForm.email) === false) && (paramDataForm.email != "")) {
            setStatusModal("error");
            setNoidungAlertValid("Sai định dạng email!")
            setOpenAlert(true)
            return false
        }
        if ((filterPhone.test(paramDataForm.soDienThoai) === false) && filterPhone2.test(paramDataForm.soDienThoai) === false) {
            setStatusModal("error");
            setNoidungAlertValid("Sai định dạng số điện thoại!")
            setOpenAlert(true)
            return false
        }
        if (paramDataForm.diaChi === "") {
            setStatusModal("error");
            setNoidungAlertValid("Địa chỉ cần được nhập vào!")
            setOpenAlert(true)
            return false
        }
        return true

    }
    useEffect(() => {
        // gọi api load drink khi load trang
        getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinksList(data);
                setDrinkValue(data[0].maNuocUong)
            })
            .catch((error) => {
                setStatusModal("error");
                setNoidungAlertValid("Tải select drink thất bại!")
                setOpenAlert(true)
            })
    }, [])
    return (
        <>
            <Modal
                open={openModalAdd}
                onClose={onBtnCancelClick}
                aria-labelledby="modal-modal-add"
                aria-describedby="modal-modal-add-des"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Thêm Order!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Kích cỡ:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="size-select"
                                                defaultValue="S"
                                                style={{ width: 195 }}
                                                onChange={onSelectSizeChange}
                                            >
                                                <MenuItem value="S">S</MenuItem>
                                                <MenuItem value="M">M</MenuItem>
                                                <MenuItem value="L">L</MenuItem>
                                            </Select>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Đường kính:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input align="center" readOnly value={duongKinh} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Sườn nướng:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={suonNuong} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Salad:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={salad} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Số lượng nước:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={soLuongNuoc} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Đơn giá (VNĐ):</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={donGia} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Loại pizza:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="pizza-type-select"
                                                defaultValue="Seafood"
                                                style={{ width: 195 }}
                                                onChange={onTypePizzaChange}
                                            >
                                                <MenuItem value="Seafood">Hải sản</MenuItem>
                                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                                <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                            </Select>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Mã voucher:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onMaVoucherChange} type="number" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Loại nước uống:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="drink-type-select"
                                                defaultValue={drinkValue}
                                                style={{ width: 195 }}
                                                onChange={onDrinkTypeChange}
                                            >
                                                {drinksList.map((drink, index, productNew) => (
                                                    <MenuItem value={drink.maNuocUong} key={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                                ))}
                                            </Select>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Họ và tên:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onHoVaTenChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Email:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onEmailChange} type="email" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Số điện thoại:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onSoDienThoaiChange} type="number" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Địa chỉ:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onDiaChiChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <label>Lời nhắn:</label>
                                        </Col>
                                        <Col sm="8">
                                            <Input onChange={onLoiNhanChange} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onCreateOrderClick} className="bg-success w-75 text-white">Tạo đơn hàng</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default AddOrderModal