import { Alert, Button, Modal, Snackbar, Typography } from "@mui/material"
import { Box } from "@mui/system"
import { Col, Row } from "reactstrap"
import { useEffect, useState } from "react";
function DeleteOrderModal({ idDelete, refeshPage, openModalDelete, handleCloseDeleteModal, style, setValueFilter, setValueFilterToEmpty }) {

    const onBtnCancelClick = () => {
        handleCloseDeleteModal()
    }
    const [openAlert, setOpenAlert] = useState(false) // đóng mở alert
    const [noidungAlertValid, setNoidungAlertValid] = useState("") // nội dung alert
    const [statusModal, setStatusModal] = useState("error") // trạng thái error hay success
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const onDeleteOrderClick = () => {
        // gọi api delete order
        const vURL_DELETE = 'http://42.115.221.44:8080/devcamp-pizza365/orders/' + idDelete
        fetch(vURL_DELETE, { method: 'DELETE' })
            .then(async response => {
                const isJson = response.headers.get('content-type')?.includes('application/json');
                const data = isJson && await response.json();
                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response status
                    const error = (data && data.message) || response.status;
                    return Promise.reject(error);
                }
                handleCloseDeleteModal()
                setStatusModal("success")
                setNoidungAlertValid("Xóa User " + idDelete + " thành công!")
                setValueFilter("")
                setValueFilterToEmpty("")
                refeshPage()
                setOpenAlert(true)
            })
            .catch(error => {
                handleCloseDeleteModal()
                setStatusModal("error")
                setNoidungAlertValid("Xóa User " + idDelete + " thất bại!")
                setOpenAlert(true)
            });
    }
    return (
        <>
            <Modal
                open={openModalDelete}
                onClose={onBtnCancelClick}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"

            >
                <Box sx={style} style={{ backgroundColor: "white" }}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Delete Order!</strong>
                    </Typography>
                    <Row>
                        <h3>Bạn đồng ý xóa đơn hàng có Id là <strong className="text-danger">{idDelete}</strong> chứ?</h3>
                    </Row>
                    <Row className="mt-4 text-center" >
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onDeleteOrderClick} className="bg-danger w-75 text-white">Chấp nhận xóa</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default DeleteOrderModal