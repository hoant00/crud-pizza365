import {TextField } from "@mui/material"
function FilterName({ onInputFilterChangeHandler, valueFilterToEmpty, setValueFilterToEmpty }) {
    const onInputFilterChange = (event) => {
        setValueFilterToEmpty(event.target.value)
        onInputFilterChangeHandler(event.target.value)
    }
    return (
        <>
            <TextField onChange={onInputFilterChange}
                type="text"
                className="borderRadius-Texfield ms-4" color="warning"
                label="Filter Name" variant="outlined"
                value={valueFilterToEmpty}
            />
        </>
    )
}

export default FilterName