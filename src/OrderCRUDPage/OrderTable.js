import { Alert, Button, Container, Grid, Pagination, Paper, Snackbar, styled, Table, TableBody, TableCell, tableCellClasses, TableContainer, TableHead, TableRow } from "@mui/material"
import { useEffect, useState } from "react";
import AddOrderModal from "./AddOrderModal"
import EditOrderModal from "./EditOrderModal"
import FilterName from "./FilterName"
import DeleteOrderModal from "./DeleteOrderModal"
import { Col } from "reactstrap";
function OrderTable() {
    var filterFirstTime = false;
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };
    const StyledTableCell = styled(TableCell)(({ theme }) => ({
        [`&.${tableCellClasses.head}`]: {
            backgroundColor: theme.palette.common.black,
            color: theme.palette.common.white,
        },
        [`&.${tableCellClasses.body}`]: {
            fontSize: 14,
        },
    }));

    const StyledTableRow = styled(TableRow)(({ theme }) => ({
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
        // hide last border
        '&:last-child td, &:last-child th': {
            border: 0,
        },
    }));
    const [openAlert, setOpenAlert] = useState(false) // đóng mở alert
    const [noidungAlertValid, setNoidungAlertValid] = useState("") // nội dung alert
    const [statusModal, setStatusModal] = useState("error") // trạng thái error hay success
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const [orders, setOrders] = useState([]); // mảng lưu trữ đơn hàng khi gọi api, mảng này sẽ không thay đổi
    const [orderAfterFilter, setOrderAfterFilter] = useState([]); // mảng sau khi được lọc, sẽ thay đổi khi lọc
    const [varRefeshPage, setVarRefeshPage] = useState(0); // biến thay đổi thì sẽ refesh page
    const [openModalAdd, setOpenModalAdd] = useState(false); // trạng thái modal thêm đơn hàng
    const [openModalEdit, setOpenModalEdit] = useState(false); // trạng thái modal sửa đơn hàng
    const [openModalDelete, setOpenModalDelete] = useState(false); // trạng thái modal sửa đơn hàng
    const [rowEdit, setRowEdit] = useState([]);
    const [idDelete, setIdDelete] = useState("");
    const [valueFilterToEmpty, setValueFilterToEmpty] = useState("")
    const [page, setPage] = useState(1);
    const [noPage, setNoPage] = useState(1);
    const limit = 10;
    const [pagiNumber, setPagiNumber] = useState(1)
    const [valueFilter, setValueFilter] = useState("")
    const refeshPage = () => {
        setVarRefeshPage(varRefeshPage + 1)
    }
    // hàm gọi api
    const getData = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }
    // hàm đóng add modal
    const handleCloseAddModal = () => setOpenModalAdd(false);
    // hàm đóng edit modal
    const handleCloseEditModal = () => setOpenModalEdit(false);
    // hàm đóng delete modal
    const handleCloseDeleteModal = () => setOpenModalDelete(false);

    const onBtnAddOrderClick = () => {
        setOpenModalAdd(true)
        
    }
    const onBtnEditClick = (paramRow) => {
        setOpenModalEdit(true)
        setRowEdit(paramRow)
        
    }
    const onBtnDeleteClick = (paramRow) => {
        setOpenModalDelete(true)
        setIdDelete(paramRow.id)
        
    }
    const onInputFilterChange = (paramValue) => {
        setPagiNumber(1)
        setValueFilter(paramValue)
        filterDataByName(paramValue)
        setPage(1);
    }
    const filterDataByName = (paramValue) => {
        const fillData = []; // biến lọc mảng có bị null không
        orders.map((order, index, orders) => { // vòng lặp lọc mảng có null hay không để sử dụng toLowerCase
            if (order.hoTen !== null && order.hoTen !== undefined) {
                fillData.push(order)
            }
        })
        const subData = [];
        fillData.map((rowData, index, fillData) => {
            if ((rowData.hoTen.toLowerCase()).includes(paramValue.toLowerCase())) {
                subData.push(rowData)
            }
        })
        setNoPage(Math.ceil(subData.length / limit));
        setOrderAfterFilter(subData.slice((page - 1) * limit, page * limit));
        console.log(page)
    }
    const changeHandlerComponent = (event, value) => {
        setPagiNumber(value);
        setPage(value);
        
    }
    
    useEffect(() => {
        if(valueFilter != ""){
            filterDataByName(valueFilter)
        }
        else{
            // gọi api lấy thông tin tất cả order
        getData("http://42.115.221.44:8080/devcamp-pizza365/orders")
        .then((data) => {
            setOrders(data);
            //setOrderAfterFilter(data);
            setNoPage(Math.ceil(data.length / limit));
            setOrderAfterFilter(data.slice((page - 1) * limit, page * limit));
            setValueFilterToEmpty("");
            
        })
        .catch((error) => {
            setStatusModal("error")
            setNoidungAlertValid("Load Orders thất bại! Kiểm tra mạng!")
            setOpenAlert(true)
        })
        }
        
    }, [varRefeshPage, page])
    return (
        <>
            <Container maxWidth="xl">
                <Button onClick={onBtnAddOrderClick} sx={{ mb: 1 }} value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Create Order +</Button>
                <FilterName setValueFilterToEmpty={setValueFilterToEmpty} valueFilterToEmpty={valueFilterToEmpty} onInputFilterChangeHandler={onInputFilterChange} />
                <Grid container>
                    <Grid item xs={12} md={12} lg={12}>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }} aria-label="orders table">
                                <TableHead>
                                    <TableRow style={{ backgroundColor: "#ffeb3b" }}>
                                        <TableCell align="center">OrderId</TableCell>
                                        <TableCell align="center">Kích cỡ</TableCell>
                                        <TableCell align="center">Loại Pizza</TableCell>
                                        <TableCell align="center">Nước uống</TableCell>
                                        <TableCell align="center">Đơn giá</TableCell>
                                        <TableCell align="center">Họ và tên</TableCell>
                                        <TableCell align="center">Số điện thoại</TableCell>
                                        <TableCell align="center">Trạng thái</TableCell>
                                        <TableCell align="center">Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {orderAfterFilter.map((row, index) => (
                                        <StyledTableRow key={index + "row"} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                            <StyledTableCell align="center">{row.orderId}</StyledTableCell>
                                            <StyledTableCell align="center">{row.kichCo}</StyledTableCell>
                                            <StyledTableCell align="center">{row.loaiPizza}</StyledTableCell>
                                            <StyledTableCell align="center">{row.idLoaiNuocUong}</StyledTableCell>
                                            <StyledTableCell align="center">{row.thanhTien}</StyledTableCell>
                                            <StyledTableCell align="center">{row.hoTen}</StyledTableCell>
                                            <StyledTableCell align="center">{row.soDienThoai}</StyledTableCell>
                                            <StyledTableCell align="center">{row.trangThai}</StyledTableCell>
                                            <StyledTableCell align="center">
                                                <Button value={index + "a"} onClick={() => { onBtnEditClick(row) }} style={{ borderRadius: 25, backgroundColor: "#2196f3", padding: "10px 20px", fontSize: "10px" }} variant="contained">Edit</Button>
                                                <Button value={index + "b"} onClick={() => { onBtnDeleteClick(row) }} style={{ borderRadius: 25, backgroundColor: "#f50057", padding: "10px 20px", fontSize: "10px" }} variant="contained">Delete</Button>
                                            </StyledTableCell>
                                        </StyledTableRow >
                                    ))}

                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
                <Col sm="12" style={{ display: "flex" }}>
                    <div style={{ marginLeft: "auto" }}>
                        <Grid item xs={12} md={12} sm={12} lg={12} marginTop={5} marginBottom={5}>
                            <Pagination page={pagiNumber} onChange={changeHandlerComponent} count={noPage}></Pagination>
                        </Grid>
                    </div>
                </Col>
                
            </Container>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
            <AddOrderModal setValueFilter={setValueFilter} setValueFilterToEmpty={setValueFilterToEmpty}  refeshPage={refeshPage} openModalAdd={openModalAdd} handleCloseAddModal={handleCloseAddModal} style={style} getData={getData} />
            <EditOrderModal setValueFilter={setValueFilter} setValueFilterToEmpty={setValueFilterToEmpty} rowEdit={rowEdit} refeshPage={refeshPage} openModalEdit={openModalEdit} handleCloseEditModal={handleCloseEditModal} style={style} getData={getData} />
            <DeleteOrderModal setValueFilter={setValueFilter} setValueFilterToEmpty={setValueFilterToEmpty} idDelete={idDelete} refeshPage={refeshPage} openModalDelete={openModalDelete} handleCloseDeleteModal={handleCloseDeleteModal} style={style} getData={getData} />
        </>
    )
}

export default OrderTable