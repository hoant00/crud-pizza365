
import { TextField, Typography } from "@mui/material"
import OrderTable from "./OrderTable"
function OrderTableCRUDPage () {
    return(
        <>
            <Typography sx={{textAlign: "center", fontSize: '2.5rem', fontWeight: "700", mt: 2}}>Order List</Typography>
            <OrderTable/>
        </>
    )
}

export default OrderTableCRUDPage