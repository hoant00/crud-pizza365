import { Alert, Box, Button, Input, MenuItem, Modal, Select, Snackbar, Typography } from "@mui/material"
import { Col, Label, Row } from "reactstrap"
import { useEffect, useState } from "react";
function EditOrderModal({ refeshPage, openModalEdit, handleCloseEditModal, style, getData, rowEdit, setValueFilter, setValueFilterToEmpty }) {
    const [drinksList, setDrinksList] = useState([])

    //khai báo các state
    const [id, setId] = useState("")
    const [orderId, setOrderId] = useState("")
    const [giamGia, setGiamGia] = useState(0)
    const [phaiThanhToan, setPhaiThanhToan] = useState(0)
    const [ngayTao, setNgayTao] = useState("")
    const [ngayCapNhat, setNgayCapNhat] = useState("")

    const [size, setSize] = useState("")
    const [duongKinh, setDuongKinh] = useState("")
    const [suonNuong, setSuonNuong] = useState("")
    const [salad, setSalad] = useState("")
    const [soLuongNuoc, setSoLuongNuoc] = useState("")
    const [donGia, setDonGia] = useState(0)
    const [loaiPizza, setLoaiPizza] = useState(null)
    const [maVoucher, setMaVoucher] = useState(0)
    const [loaiNuocUong, setLoaiNuocUong] = useState("")
    const [disPlayAnounce, setDisPlayAnounce] = useState("")
    const [hoVaTen, setHoVaTen] = useState("")
    const [email, setEmail] = useState("")
    const [soDienThoai, setSoDienThoai] = useState("")
    const [diaChi, setDiaChi] = useState("")
    const [loiNhan, setLoiNhan] = useState("")
    const [statusOrder, setStatusOrder] = useState(null)

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseEditModal()
    }
    const onEditOrderClick = () => {
        // thu thập dữ liệu
        var getDataFrom = {
            trangThai: statusOrder
        }
        // valid dữ liệu
        var vCheckStatusOrder = validStatusOrder(getDataFrom.trangThai)

        if (vCheckStatusOrder) {
            // gọi api sửa trạng thái đơn hàng
            const body = {
                method: 'PUT',
                body: JSON.stringify(getDataFrom),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                }
            }
            getData("http://42.115.221.44:8080/devcamp-pizza365/orders/" + id, body)
                .then((data) => {
                    handleCloseEditModal()
                    setStatusModal("success");
                    setNoidungAlertValid("Sửa đơn hàng thành công! Chờ 3s để thấy sự cập nhật");
                    setOpenAlert(true);
                    setValueFilter("")
                    setValueFilterToEmpty("")
                    refeshPage()
                })
                .catch((error) => {
                    setStatusModal("error");
                    setNoidungAlertValid("Sửa đơn hàng thất bại!")
                    setOpenAlert(true)
                })
        }

    }
    // hàm valid trạng thái xem có bị null hoặc rỗng hay không
    const validStatusOrder = (paramStatusOrder) => {
        if (paramStatusOrder === "open" || paramStatusOrder === "cancel" || paramStatusOrder === "confirmed") {
            return true
        }
        else {
            setStatusModal("error");
            setNoidungAlertValid("Trạng thái đơn hàng không đúng! Hãy chọn lại!")
            setOpenAlert(true)
            return false
        }
    }
    // khi trạng thái đơn hàng thay đổi sẽ set lại state
    const onStatusOrderChange = (event) => {
        setStatusOrder(event.target.value)
    }
    // hàm check loại nước uống có bị null hay "" hay undefined hay không
    const checkValueDrink = (data) => {
        if (rowEdit.idLoaiNuocUong !== null && rowEdit.idLoaiNuocUong !== "" && rowEdit.idLoaiNuocUong !== undefined) {
            for (let i = 0; i < data.length; i++) {
                if (data[i].maNuocUong.toLowerCase() === rowEdit.idLoaiNuocUong.toLowerCase()) {
                    return setDisPlayAnounce("none")
                }
            }
            return setDisPlayAnounce("block")
        }
        else {
            return setDisPlayAnounce("block")
        }

    }
    useEffect(() => {
        // gọi api lấy danh sách drink
        getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                setDrinksList(data);
                checkValueDrink(data)
            })
            .catch((error) => {
                setStatusModal("error");
                setNoidungAlertValid("Tải select drink thất bại!")
                setOpenAlert(true)
            })
        // set các state khi load trang
        setId(rowEdit.id)
        setOrderId(rowEdit.orderId)
        setGiamGia(rowEdit.giamGia)
        setPhaiThanhToan(parseInt(rowEdit.thanhTien) - rowEdit.giamGia)
        setNgayTao(rowEdit.ngayTao)
        setNgayCapNhat(rowEdit.ngayCapNhat)

        setSize((rowEdit.kichCo === null || rowEdit.kichCo === "" || rowEdit.kichCo === undefined) ? null : (rowEdit.kichCo).toLowerCase())
        setDuongKinh(rowEdit.duongKinh)
        setSuonNuong(rowEdit.suon)
        setSalad(rowEdit.salad)
        setSoLuongNuoc(rowEdit.soLuongNuoc)
        setDonGia(parseInt(rowEdit.thanhTien))


        setLoaiPizza((rowEdit.loaiPizza === null || rowEdit.loaiPizza === "" || rowEdit.loaiPizza === undefined) ? null : (rowEdit.loaiPizza).toLowerCase())
        setMaVoucher(rowEdit.idVourcher)
        setLoaiNuocUong(rowEdit.idLoaiNuocUong)
        setHoVaTen(rowEdit.hoTen)
        setEmail(rowEdit.email)
        setSoDienThoai(rowEdit.soDienThoai)
        setDiaChi(rowEdit.diaChi)
        setLoiNhan(rowEdit.loiNhan)
        setStatusOrder((rowEdit.trangThai === null || rowEdit.trangThai === "" || rowEdit.trangThai === undefined) ? null : (rowEdit.trangThai).toLowerCase())

    }, [openModalEdit])
    return (
        <>
            <Modal
                open={openModalEdit}
                onClose={onBtnCancelClick}
                aria-labelledby="modal-edit"
                aria-describedby="modal-edit-order"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-edit-title" variant="h5" component="h2">
                        <strong>Edit Order!</strong>
                    </Typography>
                    <Row style={{ marginBottom: 10 }}>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>ID:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={id} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Order ID:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={orderId} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Kích cỡ:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="size-select"
                                                value={size ? size.toUpperCase() : ""}
                                                defaultValue={size ? size.toUpperCase() : ""}
                                                style={{ width: 195, height: 30 }}
                                                readOnly
                                            >
                                                <MenuItem value="S">S</MenuItem>
                                                <MenuItem value="M">M</MenuItem>
                                                <MenuItem value="L">L</MenuItem>
                                            </Select><br></br>
                                            {!(size === "s" || size === "m" || size === "l") ? <i style={{ fontSize: "10px", color: "red" }}>Kích cỡ không có trong select!</i> : null}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Đường kính:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input align="center" readOnly value={duongKinh} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Sườn nướng:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={suonNuong} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Salad:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={salad} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Số lượng nước:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={soLuongNuoc} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Đơn giá (VNĐ):</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={donGia} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Loại pizza:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="pizza-type-select"
                                                value={loaiPizza ? loaiPizza.charAt(0).toUpperCase() + loaiPizza.slice(1) : ""}
                                                defaultValue={loaiPizza ? loaiPizza.charAt(0).toUpperCase() + loaiPizza.slice(1) : ""}
                                                style={{ width: 195, height: 30 }}
                                                readOnly
                                            >
                                                <MenuItem value="Seafood">Hải sản</MenuItem>
                                                <MenuItem value="Hawaii">Hawaii</MenuItem>
                                                <MenuItem value="Bacon">Thịt hun khói</MenuItem>
                                            </Select><br></br>
                                            {!(loaiPizza === "seafood" || loaiPizza === "hawaii" || loaiPizza === "bacon") ? <i style={{ fontSize: "10px", color: "red" }}>Loại pizza không có trong select! <br></br> Seafood-Hawaii-Bacon</i> : null}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Mã voucher:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={maVoucher} readOnly type="number" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Loại nước uống:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="drink-type-select"
                                                value={loaiNuocUong ? loaiNuocUong.toUpperCase() : ""}
                                                defaultValue={loaiNuocUong ? loaiNuocUong.toUpperCase() : ""}
                                                style={{ width: 195, height: 30 }}
                                                readOnly
                                            >
                                                {drinksList.map((drink, index, productNew) => (
                                                    <MenuItem value={drink.maNuocUong} key={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                                ))}
                                            </Select><br></br>
                                            <i style={{ fontSize: "10px", color: "red", display: disPlayAnounce }}>Loại nước uống không có trong select!</i>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Họ và tên:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={hoVaTen} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Email:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={email} readOnly type="email" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Số điện thoại:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={soDienThoai} readOnly type="number" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Địa chỉ:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={diaChi} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Lời nhắn:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input value={loiNhan} readOnly />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Trạng thái đơn hàng:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Select
                                                id="status-select"
                                                value={statusOrder ? statusOrder : ""}
                                                defaultValue={statusOrder ? statusOrder : ""}
                                                style={{ width: 195, height: 30, backgroundColor: "yellow" }}
                                                onChange={onStatusOrderChange}
                                            >
                                                <MenuItem value="open">Open</MenuItem>
                                                <MenuItem value="cancel">Đã hủy</MenuItem>
                                                <MenuItem value="confirmed">Đã xác nhận</MenuItem>
                                            </Select><br></br>
                                            {!(statusOrder === "open" || statusOrder === "cancel" || statusOrder === "confirmed") ? <i style={{ fontSize: "10px", color: "red" }}>Trạng thái không có trong select! <br></br> open-cancel-confirmed</i> : null}
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Được giảm (VNĐ):</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={giamGia} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Phải thanh toán:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={phaiThanhToan} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                            <Row >
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Ngày tạo:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={ngayTao} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-2">
                        <Col sm="6">
                            <Row>
                                <Col sm="12">
                                    <Row>
                                        <Col sm="4">
                                            <Label>Ngày cập nhật:</Label>
                                        </Col>
                                        <Col sm="8">
                                            <Input readOnly value={ngayCapNhat} />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="text-center">
                        <Col sm="12">
                            <Row >
                                <Col sm="6">
                                    <Button onClick={onEditOrderClick} className="bg-success w-75 text-white">Edit Order</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}

export default EditOrderModal